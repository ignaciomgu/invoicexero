let chai = require('chai');
let expect = chai.expect;
let Item = require('../src/Item');
/**
 * Testing of Item Class. See below the descriptions in each test
 */
describe('Item', function() {
    beforeEach(function() {
        this.itemCero =  new Item(0, 6.99, 1, "Apple");
    });

    afterEach(function() {

    });

    it('addQuantity() should return the item with the new quantity added', function() {
        let initialQuantity = this.itemCero.getQuantity();
        this.itemCero.addQuantity(this.itemCero.getQuantity()+this.itemCero.getQuantity());
        expect(this.itemCero.getQuantity()).to.equal(initialQuantity+initialQuantity+initialQuantity);
        this.itemCero.addQuantity(-1);
        expect(this.itemCero.getQuantity()).to.equal(initialQuantity+initialQuantity+initialQuantity-1);
    });

    it('clone() should return a clone of the item', function() {
        let clonedItem = this.itemCero.clone();
        expect(clonedItem===this.itemCero).to.equal(false);
        expect(clonedItem.getId()).to.equal(this.itemCero.getId());
        expect(clonedItem.getTotalPrice()).to.equal(this.itemCero.getTotalPrice());
        expect(clonedItem.getDescription()).to.equal(this.itemCero.getDescription());
    });
});
