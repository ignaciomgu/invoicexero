let chai = require('chai');
let expect = chai.expect;
let Invoice = require('../src/Invoice');
let Item = require('../src/Item');
/**
 * Testing of Invoice Class. See below the descriptions in each test
 */
describe('Invoice', function() {
	beforeEach(function() {
		this.itemCero =  new Item(0, 6.99, 1, "Apple");
        this.itemOne =  new Item(1, 10.21, 4, "Banana");
        this.itemTwo =  new Item(2, 5.21, 1, "Orange" );
        this.itemThree =  new Item(3, 6.21, 5, "Pineapple");
        this.itemFour =  new Item(4, 10.00, 1, "Blueberries");
        this.itemFive =  new Item(5, 0.99, 5, "Onion");
        this.itemSix =  new Item(6, 10.49, 2, "Watermelon");
        this.itemSeven =  new Item(7, 1.99, 20, "Peer");
    });

	afterEach(function() {

	});

	it('getItems() should return 1 if _items are passed in', function() {
        let invoice = new Invoice(1);
        invoice.addItem(this.itemCero);
		expect(invoice.getItems().size).to.equal(1);
        expect(invoice.getItems().get(this.itemCero.getId())).to.equal(this.itemCero);
	});

    it('getItems() should return 3 if _items are passed in', function() {
        let invoice = new Invoice(1);
        invoice.addItem(this.itemCero);
        invoice.addItem(this.itemOne);
        invoice.addItem(this.itemThree);
        expect(invoice.getItems().size).to.equal(3);
        expect(invoice.getItems().get(this.itemCero.getId())).to.equal(this.itemCero);
        expect(invoice.getItems().get(this.itemOne.getId())).to.equal(this.itemOne);
        expect(invoice.getItems().get(this.itemThree.getId())).to.equal(this.itemThree);
    });

    it('removeItemById(itemId) should remove the item passed by itemId parameters', function() {
        let invoice = new Invoice(1);
        invoice.addItem(this.itemOne);
        invoice.addItem(this.itemThree);
        invoice.removeItemById(this.itemOne.getId());
        expect(invoice.getItems().size).to.equal(1);
        expect(invoice.getItems().get(this.itemThree.getId())).to.equal(this.itemThree);
    });

    it('merge(invoice) should merge the invoice passed by parameters with the currently invoice', function() {
        let invoice = new Invoice(1);
        invoice.addItem(this.itemFour);
        let sizeOfItems = invoice.getItems().size;
        let totalPrice = invoice.getTotal();

        let invoiceToMerge = new Invoice();
        invoiceToMerge.addItem(this.itemOne);
        invoiceToMerge.addItem(this.itemTwo);

        invoice.merge(invoiceToMerge);

        expect(invoice.getItems().size).to.equal(sizeOfItems+invoiceToMerge.getItems().size);
        expect(invoice.getTotal()).to.equal(totalPrice+invoiceToMerge.getTotal());
    });

    it('sameProperties(invoice) should return true if invoice passed by parameters have the same values in the properties of currently invoice', function() {
        let invoice = new Invoice(1);
        invoice.addItem(this.itemOne);
        invoice.addItem(this.itemTwo);

        let invoiceToCompare = new Invoice();
        invoiceToCompare.addItem(this.itemOne);
        invoiceToCompare.addItem(this.itemTwo);

        let areNotTheSame = invoice.sameProperties(invoiceToCompare);

        let invoiceToCompare1 = new Object();
        invoiceToCompare1._id = invoice.getId();
        invoiceToCompare1._items = invoice.getItems();
        invoiceToCompare1._totalPrice = invoice.getTotal();
        invoiceToCompare1._createdDate = invoice.getCreationDate();
        invoiceToCompare1._lastModificationDate = invoice.getLastModificationDate();

        let areTheSame = invoice.sameProperties(invoiceToCompare1);

        expect(areNotTheSame).to.equal(false);
        expect(areTheSame).to.equal(true);
    });


    it('clone() should return a clone of the invoice', function() {
        let invoice = new Invoice(1);
        invoice.addItem(this.itemOne);
        let clonedInvoice = invoice.clone();
        let equalsObjects = invoice.sameProperties(clonedInvoice);
        expect(clonedInvoice===invoice).to.equal(false);
        expect(equalsObjects).to.equal(true);
    });

    it('convertToString() should a string version of the invoice passed by parameters', function() {
        let invoice = new Invoice(1);
        invoice.addItem(this.itemFive);
        invoice.addItem(this.itemSix);
        let stringExpected = "{\"_id\":1,\"_items\":\"[[5,{\\\"_id\\\":5,\\\"_quantity\\\":5,\\\"_price\\\":0.99,\\\"_description\\\":\\\"Onion\\\"}],[6,{\\\"_id\\\":6,\\\"_quantity\\\":2,\\\"_price\\\":10.49,\\\"_description\\\":\\\"Watermelon\\\"}]]\",\"_totalPrice\":25.93,\"_createdDate\":creationDate,\"_lastModificationDate\":modificationDate}";
        stringExpected = stringExpected.replace("creationDate", JSON.stringify(invoice.getCreationDate()));
        stringExpected = stringExpected.replace("modificationDate", JSON.stringify(invoice.getLastModificationDate()));
        let invoiceString = invoice.convertToString();

        expect(invoiceString).to.equal(stringExpected);
    });

        it('convertToObject() should a object version of the invoice string passed by parameters', function() {
        let invoice = new Invoice(1);
        invoice.addItem(this.itemFive);
        invoice.addItem(this.itemSeven);

        let invoiceString = invoice.convertToString();
        let invoiceObject = Invoice.convertToObject(invoiceString);
        let equalsObjects = invoice.sameProperties(invoiceObject);

        expect(equalsObjects).to.equal(true);
        expect(invoice === invoiceObject).to.equal(false);
    });

	it('getTotal() should return the sum of the price * quantity for all _items', function() {
        let invoice = new Invoice(1);
        invoice.addItem(this.itemCero);
        invoice.addItem(this.itemOne);
		expect(invoice.getTotal().toFixed(2)).to.equal(((this.itemCero.getTotalPrice()) + this.itemOne.getTotalPrice()).toFixed(2));
	});

    it('getTotal() should return the sum of the price * quantity for all _items. Adding three _items and then deleting one of them', function() {
        let invoice = new Invoice(1);
        invoice.addItem(this.itemCero);
        invoice.addItem(this.itemOne);
        invoice.addItem(this.itemThree);
        invoice.removeItemById(this.itemThree.getId());
        expect(invoice.getTotal().toFixed(2)).to.equal(((this.itemCero.getTotalPrice()) + this.itemOne.getTotalPrice()).toFixed(2));
    });

    it('getTotal() should return the sum of the price * quantity for all _items. Adding two _items with the same id but different quantity', function() {
        let initialQuantity = this.itemOne.getQuantity();
    	let invoice = new Invoice(1);
        invoice.addItem(this.itemOne);
        let clonedItem = this.itemOne.clone();
        clonedItem.addQuantity(this.itemOne.getQuantity());
        invoice.addItem(clonedItem);
        expect(invoice.getTotal().toFixed(2)).to.equal(((initialQuantity*3)*this.itemOne.getPrice()).toFixed(2));
    });
});
