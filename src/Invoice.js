/**
 * Represent a Invoice entity
 */
class Invoice{

		/**
		 * Class constructor
		 * @param id
		 */
		constructor(id) {
			this._id = id;
            this._items = new Map();
            this._totalPrice = 0;
			this._createdDate = new Date();
            this._lastModificationDate = new Date();
		}

		/**
		 * Adding new item passed by parameter (if the item exist, then, the function only increase the number of items)
		 * and update total price and last modification date of the Invoice.
		 * @param item
		 */
		addItem(item) {
            let existentItem = this._items.get(item.getId());
			if(typeof existentItem !== "undefined"){
			    this._items.set(item.getId(),existentItem.addQuantity(item.getQuantity()));
			}else{
                this._items.set(item.getId(),item);
			}
            this._totalPrice += (item.getTotalPrice());
			this._lastModificationDate = new Date();
		};

		/**
		 * Removing one item by id passed by parameter and update the total price and last modification date of the Invoice
		 * @param itemId
		 */
		removeItemById(itemId) {
            let existentItem = this._items.get(itemId);
            if(typeof existentItem !== "undefined"){
                this._totalPrice = this._totalPrice - (existentItem.getTotalPrice());
                this._items.delete(itemId);
            }
            this._lastModificationDate = new Date();
		};

		/**
		 * Adding the items of the Invoice passed by parameter to current Invoice and update the last modification date of the current Invoice
		 * @param invoiceToMerge
		 */
		merge(invoiceToMerge) {
            invoiceToMerge.getItems().forEach(item => {
                this.addItem(item);
            });
            this._lastModificationDate = new Date();
		}

		/**
		 * Creating a object copy of the Invoice (new object instance with the same properties)
		 * @returns {Invoice}
		 */
		clone() {
            let invoice = new Invoice(this._id);
            invoice._items = this._items;
            invoice._totalPrice = this._totalPrice;
            invoice._createdDate = this._createdDate;
            invoice._lastModificationDate = this._lastModificationDate;
            return invoice;
		};

		/**
		 * Returns the total price of the Invoice (a sum of items price and quantities)
		 * @returns {number}
		 */
		getTotal() {
            return this._totalPrice;
		}

		/**
		 * Returns the list of the items Map<Item Id, Item Object>
		 * @returns {Map<int, Item>}
		 */
		getItems(){
			return this._items;
		}

		/**
		 * Returns the creation date of the Invoice
		 * @returns {Date}
		 */
		getCreationDate(){
			return this._createdDate;
		}

		/**
		 * Returns the last modification date of the Invoice
		 * @returns {Date}
		 */
		getLastModificationDate(){
			return this._lastModificationDate;
		}

		/**
		 * Returns the identification of the Invoice
		 * @returns {number}
		 */
		getId(){
			return this._id;
		}

		/**
		 * Return a String in JSON Format of the current Invoice
		 * @returns {string}
		 */
		convertToString(){
			let objectInvoice = new Object();
            objectInvoice._id = this._id;
            objectInvoice._items = JSON.stringify(Array.from(this._items.entries()));
            objectInvoice._totalPrice = this._totalPrice;
            objectInvoice._createdDate = this._createdDate;
            objectInvoice._lastModificationDate = this._lastModificationDate;
            return JSON.stringify(objectInvoice);
        }

		/**
		 * Makes a comparision property to property between current Invoice and Invoice passed by parameter
		 * @param invoice to compare
		 * @returns {boolean}
		 */
		sameProperties(invoice){
			return invoice._totalPrice===this._totalPrice&&
                   invoice._id===this._id&&
                   JSON.stringify(Array.from(invoice._items.entries()))===JSON.stringify(Array.from(this.getItems().entries()))&&
                   invoice._createdDate.getTime()===this._createdDate.getTime()&&
			       invoice._lastModificationDate.getTime() === this._lastModificationDate.getTime();
		}

		/**
		 * Converts a Invoice String passed by parameter to a Invoice Object
		 * @param invoiceString
		 * @returns {Invoice}
		 */
		static convertToObject(invoiceString){
			let objectInvoice = JSON.parse(invoiceString);
			let invoice = new Invoice(objectInvoice._id);
            invoice._totalPrice = objectInvoice._totalPrice;
			invoice._items = new Map(JSON.parse(objectInvoice._items));
            invoice._createdDate.get = new Date(objectInvoice._createdDate);
            invoice._lastModificationDate = new Date(objectInvoice._lastModificationDate);
			return invoice;
		}
}

module.exports = Invoice;
