/**
 * Represent a item product entity
 */
class Item{

    /**
     * Class constructor
     * @param id
     * @param price
     * @param quantity
     * @param description
     */
    constructor(id, price, quantity, description) {
        //Identifier
        this._id = id;
        //Number of items
        this._quantity = quantity;
        //Price of each item
        this._price = price;
        //Information about the item
        this._description = description;
    }

    /**
     * Increasing or decrease number of item
     * @param quantity
     * @returns {Item}
     */
    addQuantity(quantity){
        this._quantity+=quantity;
        return this;
    }

    /**
     * Creating a object copy of the item (new object instance with the same properties)
     * @returns {Item}
     */
    clone() {
        return new Item(this._id,this._price,this._quantity,this._description);
    };

    /**
     * Returns id property of the item
     * @returns {int}
     */
    getId(){
        return this._id;
    }

    /**
     * Returns number of items
     * @returns {number}
     */
    getQuantity(){
        return this._quantity;
    }

    /**
     * Returns price of the item
     * @returns {number}
     */
    getPrice(){
        return this._price;
    }

    /**
     * Returns the description of the item
     * @returns {string}
     */
    getDescription(){
        return this._description;
    }

    /**
     * Returns the price of each item
     * @returns {number}
     */
    getTotalPrice(){
        return this._quantity * this._price;
    }

}

module.exports = Item;
