# Invoice Xero

- Clone the project or download the zip.

- Open a console into project directory and run this commands (it will install libraries and run the tests):

```
npm install
mocha tests --recursive --watch
```